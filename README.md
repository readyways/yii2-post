# **Yii 2 Post module** 

## Installation
-----------------
The best way to install this extension is through [composer](http://getcomposer.org/download/).

Either run:

```
composer require --prefer-dist it-crowd-team/yii2-post "dev-dev"
```

or add:

```json
"it-crowd-team/yii2-post" : "dev-dev"
```

to the require section of your `composer.json` file.

While extension in **dev**, do not forget to check if next lines exist in your root `composer.json` file:

```json
...
"repositories": [
	{"type": "vcs", "url": "https://bitbucket.org/it-crowd-team/yii2-post.git"}
],
...
```

# Usage

To access frontend part of the module, you need to add this to your application configuration:

```php
<?php
    ...
    'modules' => [
        'post' => [
            'class' => 'ict\posts\Module',
        ],
    ],
    ...
```

To access backend part of the module, you need to add this to your application configuration:

```php
<?php
    ...
    'modules' => [
        'post' => [
            'class' => 'ict\posts\Module',
            'appType' => 'backend',
            // You can allow access to backend only for users that have appropriate rights
            // e.g. [['allow' => \Yii::$app->user->isAdmin()]]
            'backendAccessRules' => [['allow' => true]]
        ],
    ],
    ...
```

# Run module migrations

```
$ yii migrate/up --migrationPath=@vendor/it-crowd-team/yii2-post/console/migrations
```


# Generate module translations
```
$ yii message vendor/it-crowd-team/yii2-post/messages/config.php
```

## Note 
> Please use only `post` message translation category, all over the module.

Use only your dedicated branch, do not commit in `master` or `dev`.
*...by @it-crowd-team*