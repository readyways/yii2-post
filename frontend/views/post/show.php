<?php

use yii\bootstrap\Html;
use ict\posts\common\components\BaseHelper;
use dosamigos\disqus\Comments;

/**
 * @var $this yii\web\View
 * @var $id string|integer
 * @var $lang string
 * @var $created string
 * @var $updated string
 * @var $published string
 * @var $title string
 * @var $slug string
 * @var $lead string
 * @var $content string
 * @var $img string
 * @var $img_desc string
 * @var $img_src string
 * @var $draft string
 * @var $enable_comments
 * @var $view_count string
 * @var $status string
 */

$this->title = $title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('post', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<article class="post-view">

    <header>
        <h1><?= Html::encode($this->title) ?></h1>
    </header>

    <?php if ($img): ?>
    <figure>
        <?= Html::img($img, ['class' => 'img-responsive']); ?>
        <?php if($img_desc || $img_src): ?>
        <figcaption>
            <?= ($img_desc) ? $img_desc : false; ?>
            <?= ($img_src) ? $img_src : false; ?>
        </figcaption>
        <?php endif; ?>
    </figure>
    <?php endif; ?>

    <?= ($lead) ? Html::tag('section', $lead, ['class' => 'lead']): false; ?>

    <?= ($content) ? Html::tag('section', $content, ['class' => 'content']): false; ?>

    <footer>
        <?= Html::icon('glyphicon glyphicon-calendar'); ?>
        <?= Html::tag('time', BaseHelper::mb_ucfirst(Yii::$app->formatter->asDate(strtotime($published), 'php:l, j F Y'))); ?>
        <?= Html::icon('glyphicon glyphicon-time'); ?>
        <?= Html::tag('time', Yii::$app->formatter->asTime(strtotime($published), 'short')); ?>
    </footer>

    <?php if ($this->context->module->enableComments && $enable_comments): ?>
    <section>
        <?= Comments::widget([
            'shortname' => $this->context->module->disqusShortname,
            'identifier' => $id
        ]); ?>
    </section>
    <?php endif; ?>

</article>
