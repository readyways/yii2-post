<?php

use yii\bootstrap\Html;
use ict\posts\common\components\BaseHelper;
?>

<article class="col-md-12">
    <hgroup>
        <h4>
            <?= Html::a($model->title, ['/post/post/show', 'slug' => $model->slug]); ?>
        </h4>
    </hgroup>

    <footer>
        <?= Html::icon('glyphicon glyphicon-calendar'); ?>
        <?= Html::tag('time', BaseHelper::mb_ucfirst(Yii::$app->formatter->asDate(strtotime($model->published), 'php:l, j F Y'))); ?>
        <?= Html::icon('glyphicon glyphicon-time'); ?>
        <?= Html::tag('time', Yii::$app->formatter->asTime(strtotime($model->published), 'short')); ?>
    </footer>

    <div>
        <?if(!empty($model->img)):?>
            <img src="/<?= $model->img ?>" alt="">
        <?endif;?>
    </div>
    <div>
        <?=$model->lead?>
    </div>
</article>
