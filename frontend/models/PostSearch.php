<?php

namespace ict\posts\frontend\models;

use ict\posts\common\models\PostSectionRelation;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use ict\posts\Module;
use ict\posts\common\models\Post;

/**
 * PostSearch represents the model behind the search form about `ict\posts\common\models\Post`.
 */
class PostSearch extends Post
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'draft', 'enable_comments', 'view_count', 'status'], 'integer'],
            [['lang', 'created', 'updated', 'published', 'title', 'slug', 'lead', 'content', 'img', 'img_desc', 'img_src', 'sections', 'countryValues'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $module = Module::getInstance();

        $query = Post::find()->published()
            ->where([
                'post.lang' => $module->getAppLanguageCode()
            ])
            ->joinWith(['sections', 'countries']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => $module->pageSize,
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['section.id' => $this->sections])
            ->andFilterWhere(['like', 'country.name_en', $this->countryValues])
            ->andFilterWhere(['>=', 'published', $this->published[0]])
            ->andFilterWhere(['<=', 'published', $this->published[1]]);

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchSection($section, $params)
    {
//        $this->rangeDateFrom = $params['PostSearch']['rangeDateFrom'];
//        $this->rangeDateTo = $params['PostSearch']['rangeDateTo'];
//        $this->sections = $params['PostSearch']['sections'];
//        $this->countryValues = $params['PostSearch']['countryValues'];

        $module = Module::getInstance();
        $query = Post::find()
            ->where([
                'post.lang' => $module->getAppLanguageCode()
            ])
            ->joinWith(['postSectionRelations' => function ($q) use ($section) {
                $q->from(['psr' => PostSectionRelation::tableName()]);
                $q->where(['psr.section_id' => $section['id']]);
            }]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'published' => SORT_DESC,
                ],
            ],
            'pagination' => [
                'pageSize' => $module->pageSize,
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            //'published' => $this->published,
            'view_count' => $this->view_count,
            'status' => $this->status,
        ]);

        //var_dump($this->published);die;
        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['>=', 'published', $this->published[0]])
            ->andFilterWhere(['<=', 'published', $this->published[1]]);


        return $dataProvider;
    }
}
