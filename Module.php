<?php

namespace ict\posts;

use Yii;

class Module extends \yii\base\Module
{
    /**
     * @var string the default route of this module.
     */
    public $defaultRoute = 'post';

    /**
     * @var integer|10 posts page size limit
     */
    public $pageSize = 10;

    /** 
     * @var boolean|true whether to use or not author behavior
     */
    public $useAuthors = true;

    /**
     * @var boolean|true whether to use or not [[2amigos/yii2-taggable-behavior]]
     */
    public $useTags = true;

    /**
     * @var boolean|true whether to use or not [[akavov/yii2-countries]]
     */
    public $useCountries = true;

    /**
     * @var string controllers path template.
     */
    private $controllersPathTemplate = 'ict\posts\%s\controllers';

    /**
     * @var string views alias path template.
     */
    private $viewsAliasTemplate = '@ict/posts/%s/views';

    /**
     * @var string application type. e.g. 'frontend' or 'backend'
     */
    public $appType = 'frontend';

    /**
     * @var array access rules for frontend part of module.
     * By default all actions are allowed.
     */
    public $frontendAccessRules = [['allow' => true]];

    /**
     * @var array access rules for backend part of module.
     * By default all actions are forbidden.
     */
    public $backendAccessRules = [];

    /**
     * @var array available post statuses.
     */
    public $postStatuses = ['Normal post', 'Hot post'];

    /**
     * @var boolean||false|string whether to use own method to get current app language.
     * To use own method pass a namespace of class which has static method [[Class::getAppLanguageCode()]]
     * @example 'app\frontend\components\Language'
     */
    public $ownLanguageGetter = false;

    /**
     * @var array list of available languages for posts.
     */
    public $languages = [
        'uk' => 'Українська',
        'ru' => 'Русский',
        'en' => 'English'
    ];

    /**
     * @var array URL and real PATH to folder, where images should be stored
     * You can set two aliases in [[common/config/bootstrap.php]], like done below:
     * ```php
     * Yii::setAlias('storageUrl', 'http://storage.example.com');
     * Yii::setAlias('storagePath', dirname(dirname(dirname(__DIR__))) . '/storage');
     * ```
     */
    public $imgPaths = [
        'url' => '@storageUrl/uploads',
        'path' => '@storagePath/uploads',
    ];

    /**
     * @var boolean|false whether to activate DISQUS comments on post pages
     */
    public $enableComments = true;

    /**
     * @var string if [[$this->enableComments]] is true, tells the Disqus service your application shortname,
     * which is the unique identifier for your website as registered on Disqus.
     * If undefined, the Disqus embed will not load.
     */
    public $disqusShortname;

    /**
     * Returns a list of behaviors that this component should behave as.
     * @return array the behavior configurations.
     */
    public function behaviors() {

        if ($this->appType === 'backend') {
            return [
                'access' => [
                    'class' => 'yii\filters\AccessControl',
                    'rules' => $this->backendAccessRules,
                ]
            ];
        } else {
            return [
                'access' => [
                    'class' => 'yii\filters\AccessControl',
                    'rules' => $this->frontendAccessRules,
                ]
            ];
        }
    }

    /**
     * Initializes the module.
     * If you override this method, please make sure you call the parent implementation.
     * @return void
     */
    public function init()
    {
        parent::init();
        $this->setControllerNamespace($this->appType);
        $this->setViewPath($this->appType);
        $this->registerTranslations();
    }

    /**
     * ControllerNamespace setter
     * @param string $appType application type
     * @return void
     */
    public function setControllerNamespace($appType)
    {
        $this->controllerNamespace = sprintf($this->controllersPathTemplate, $appType);
    }

    /**
     * Override method for parent viewPath setter
     * @param string $appType application type
     * @uses [[\yii\base\Module::setViewPath()]]
     * @return void
     */
    public function setViewPath($appType)
    {
        parent::setViewPath(sprintf($this->viewsAliasTemplate, $appType));
    }

    /**
     * Get current app language code
     * @uses [[$this->ownLanguageGetter]] to define in case you want define own language getter method
     * @return string
     */
    public function getAppLanguageCode()
    {
        if ($this->ownLanguageGetter && is_string($this->ownLanguageGetter)) {
            $class = $this->ownLanguageGetter;
            return $class::getAppLanguageCode();
        } else {
            if (preg_match('/-/', Yii::$app->language)) {
                return explode('-', Yii::$app->language)[0];
            } else {
                return Yii::$app->language;
            }
        }
    }

    /**
     * Register module translations
     * This method calling during module initialization
     * @return void
     */
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['post'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'basePath' => '@ict/posts/messages'
        ];
    }
}
