<?php

use yii\db\Migration;

class m150915_141757_create_post_section_relation_table extends Migration
{
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        echo "Creating tables.\n";
        $this->createTable('{{%post_section_relation}}', [
            'id'            => $this->primaryKey(),
            'post_id'       => $this->integer(11)->notNull(),
            'section_id'    => $this->integer(11)->notNull(),
        ], $tableOptions);

        $this->addForeignKey('post_section_relation_fk_1', '{{%post_section_relation}}', 'post_id', '{{%post}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('post_section_relation_fk_2', '{{%post_section_relation}}', 'section_id', '{{%section}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        echo "Removing foreign keys.\n";
        $this->dropForeignKey('post_section_relation_fk_1', '{{%post_section_relation}}');
        $this->dropForeignKey('post_section_relation_fk_2', '{{%post_section_relation}}');

        echo "Removing tables.\n";
        $this->dropTable('{{%post_section_relation}}');
    }
}
