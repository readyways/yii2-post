<?php

use yii\db\Migration;

class m150914_141133_create_post_tag_assn_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%post_tag_assn}}', [
            'post_id'   => $this->integer(11)->notNull(),
            'tag_id'    => $this->integer(11)->notNull(),
        ]);
        $this->addPrimaryKey('', '{{%post_tag_assn}}', ['post_id', 'tag_id']);
    }

    public function safeDown()
    {
        echo "Removing tables.\n";
        $this->dropTable('{{%post_tag_assn}}');
    }


}
