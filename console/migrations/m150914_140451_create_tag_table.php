<?php

use yii\db\Migration;

class m150914_140451_create_tag_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%tag}}', [
            'id'        => $this->primaryKey(11),
            'name'      => $this->string(255)->notNull(),
            'frequency' => $this->integer(11)->notNull()->defaultValue(0),
        ]);

    }

    public function safeDown()
    {
        echo "Removing tables.\n";
        $this->dropTable('{{%tag}}');
    }


}
