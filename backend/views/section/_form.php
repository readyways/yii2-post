<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use ict\posts\backend\components\SectionHelper;

/* @var $this yii\web\View */
/* @var $model ict\posts\common\models\Section */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs('$("#section-name").keyup(function(e){
    $.get("'.Url::to(["/{$this->context->module->id}/post/slug"]).'", {
        str: $("#section-name").val()
    }).done(function(slug){
        $("#section-slug").val(slug);
    });
});', $this::POS_END, 'slug');

?>

<div class="section-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <?= $form->field($model, 'name', ['options' => ['class' => 'col-sm-6']])->textInput(['maxlength' => true]); ?>

        <?= $form->field($model, 'slug', ['options' => ['class' => 'col-sm-6']])->textInput(['maxlength' => true]); ?>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <?= $form->field($model, 'parent_id', ['options' => ['class' => 'col-sm-6']])->dropDownList(
                    ArrayHelper::map(SectionHelper::getSectionsList($model->lang, $model->primaryKey), 'id', 'name'),
                    ['prompt' => Yii::t('post', 'Without parent')]
                ); ?>

                <?= $form->field($model, 'site_disabled', ['options' => ['class' => 'col-sm-6']])->dropDownList([Yii::t('yii', 'No'), Yii::t('yii', 'Yes')]); ?>
            </div>
            <div class="row">
                <?= $form->field($model, 'nav_disabled', ['options' => ['class' => 'col-sm-6']])->dropDownList([Yii::t('yii', 'No'), Yii::t('yii', 'Yes')]); ?>

                <?= $form->field($model, 'nav_sort', ['options' => ['class' => 'col-sm-6']])->textInput(); ?>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="row">
                <?= $form->field($model, 'description', ['options' => ['class' => 'col-sm-12']])->textarea(['rows' =>4]); ?>
            </div>
        </div>
    </div>

    <?= $form->errorSummary($model); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('post', 'Create') : Yii::t('yii', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
