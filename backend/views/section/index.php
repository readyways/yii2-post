<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;

/**
 * @var $this         yii\web\View
 * @var $searchModel  ict\posts\common\models\SectionSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $langForm     yii\base\DynamicModel
 */

$this->title = Yii::t('post', 'Sections');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="section-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <?php $form = ActiveForm::begin(['method' => 'get', 'options' => ['class' => 'col-sm-4']]); ?>

        <?= $form->field($langForm, 'lang', [
            'inputTemplate' => '<div class="input-group"><div class="input-group-btn">'.
                Html::submitButton(Yii::t('post', 'Create section'), ['class' => 'btn btn-success']).'
            </div>{input}</div>',
        ])->dropDownList($this->context->module->languages)->label(false); ?>

        <?php ActiveForm::end(); ?>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'lang',
            'name',
            'description:ntext',
            'parent_id',
            'site_disabled',
            'nav_disabled',
            'nav_sort',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
