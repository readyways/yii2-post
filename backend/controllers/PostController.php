<?php

namespace ict\posts\backend\controllers;

use ict\posts\common\models\Author;
use Yii;
use yii\base\DynamicModel;
use yii\filters\VerbFilter;
use yii\helpers\Inflector;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use ict\posts\common\models\Post;
use ict\posts\common\models\PostSearch;
use ict\posts\common\models\Tag;
use ict\posts\common\models\Country;

/**
 * PostController implements the CRUD actions for Post model.
 */
class PostController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Post models.
     * @return mixed
     */
    public function actionIndex()
    {
        $langForm = new DynamicModel(['lang']);
        $langForm->addRule('lang', 'required')
                 ->addRule('lang', 'string', ['max'=>5]);

        // Redirect user to create page with preselected language param
        if($langForm->load(Yii::$app->request->get())){
            return $this->redirect(['create', 'lang' => $langForm->lang]);
        }

        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'langForm' => $langForm
        ]);
    }

    /**
     * Displays a single Post model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Post model.
     * @param string|null $lang new section language
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionCreate($lang = null)
    {
        if($lang) {
            $model = new Post();
            $model->lang = $lang;

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            throw new BadRequestHttpException(Yii::t('post', 'The language parameter is required for this form'));
        }
    }

    /**
     * Updates an existing Post model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Post model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Convert string into URL ready string called 'slug'
     * @param string $str string that will be converted
     * @return string
     */
    public function actionSlug($str)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Inflector::slug($str);
    }

    /**
     * Finds the Post model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Post the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('yii', 'Page not found.'));
        }
    }

    /**
     * Return matched tags
     * @param  string $query
     * @return json result array
     */
    public function actionSearchTags($query = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $list = Tag::find()->select(['name'])->where(['like','name', $query])->all();
        return $list;
    }

    /**
     * Return matched authors
     * @param  string $query
     * @return json result array
     */
    public function actionSearchAuthors($query = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $list = Author::find()->select(['name'])->where(['like','name', $query])->all();
        return $list;
    }

    /**
     * Return matched countries
     * @param  string $query
     * @return json result array
     */
    public function actionSearchCountries($query = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $list = Country::find()
            //->where(['like','name_en', $query])
            //->orWhere(['like','name_uk', $query])
            //->orWhere(['like','name_ru', $query])
            ->all();
        return $list;
    }

}
