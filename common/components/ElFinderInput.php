<?php

namespace ict\posts\common\components;

use Yii;
use yii\helpers\Html;
use yii\helpers\Json;
use mihaildev\elfinder\InputFile;
use mihaildev\elfinder\AssetsCallBack;
use mihaildev\elfinder\ElFinder;

class ElFinderInput extends InputFile
{
    public $language = 'ru';
    public $filter = 'image';
    public $buttonName = '<i class="glyphicon glyphicon-folder-open"></i>';
    public $buttonOptions = ['class' => 'btn btn-info'];
    public $template = '<div class="input-group">
        <div class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></div>
        {input}
        <div class="input-group-btn">{button}</div>
    </div>';
    public $controller = '/post/elfinder';
    public $options = ['class' => 'form-control'];

    /**
     * Runs the widget.
     */
    public function run()
    {
        if ($this->hasModel()) {
            $replace['{input}'] = Html::activeTextInput($this->model, $this->attribute, $this->options);
        } else {
            $replace['{input}'] = Html::textInput($this->name, $this->value, $this->options);
        }

        $replace['{button}'] = Html::tag($this->buttonTag,$this->buttonName, $this->buttonOptions);


        echo strtr($this->template, $replace);

        AssetsCallBack::register($this->getView());

        if (!empty($this->multiple))
            $this->getView()->registerJs("mihaildev.elFinder.register(" . Json::encode($this->options['id']) . ", function(files, id){ var _f = []; for (var i in files) { _f.push(files[i].url); } \$('#' + id).val(_f.join(', ')).trigger('change'); return true;}); $(document).on('click','#" . $this->buttonOptions['id'] . "', function(){mihaildev.elFinder.openManager(" . Json::encode($this->_managerOptions) . ");});");
        else
            $this->getView()->registerJs("mihaildev.elFinder.register(" . Json::encode($this->options['id']) . ", function(file, id){ \$('#' + id).val(file.url).trigger('change');; return true;}); $(document).on('click', '#" . $this->buttonOptions['id'] . "', function(){mihaildev.elFinder.openManager(" . Json::encode($this->_managerOptions) . ");});");
    }
}
