<?php

namespace ict\posts\common\components;
use Yii;

class BaseHelper
{
    /**
     * Analog to basic php function [[ucfirst()]] that support Cyrillic
     * @param string     $str
     * @param string     $encoding
     * @param bool|false $lowerStrEnd
     * @return string
     */
     public static function mb_ucfirst($str, $encoding = "UTF-8", $lowerStrEnd = false) {
        $firstLetter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
        if ($lowerStrEnd) {
            return $firstLetter.= mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
        } else {
            return $firstLetter.= mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
        }
    }

    /**
     * Check whether post is published
     * @param  integer|string $datetime post published timestamp
     * @return boolean
     */
    public static function checkIsPublished($datetime)
    {
        if (!is_int($datetime)) {
            $datetime = strtotime($datetime);
        }
        return $datetime <= time();
    }
}