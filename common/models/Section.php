<?php

namespace ict\posts\common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%section}}".
 *
 * @property integer $id
 * @property string $lang
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property integer $parent_id
 * @property integer $site_disabled
 * @property integer $nav_disabled
 * @property integer $nav_sort
 *
 * @property PostSectionRelation[] $postSectionRelations
 */
class Section extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%section}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang', 'name', 'slug', 'site_disabled', 'nav_disabled'], 'required'],
            [['description'], 'string'],
            [['parent_id', 'description', 'nav_sort'], 'safe'],
            [['parent_id'], 'default', 'value' => 0],
            ['parent_id', 'compare', 'compareAttribute' => 'id', 'operator' => '!=', 'message' => Yii::t('post', 'Section can\'t be parent for itself')],
            [['parent_id', 'site_disabled', 'nav_disabled', 'nav_sort'], 'integer', 'min' => 0],
            [['lang'], 'string', 'max' => 5],
            [['name', 'slug'], 'string', 'max' => 255],
            [['slug'], 'unique'],
            [['lang', 'name', 'slug', 'description', 'nav_sort'], 'filter', 'filter' => 'trim'],
            [['parent_id', 'site_disabled', 'nav_disabled', 'nav_sort'], 'filter', 'filter' => 'intval'],
            [['site_disabled', 'nav_disabled'], 'in', 'range' => [0, 1]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('post', 'ID'),
            'lang' => Yii::t('post', 'Language'),
            'name' => Yii::t('post', 'Name'),
            'slug' => Yii::t('post', 'Slug'),
            'description' => Yii::t('post', 'Description'),
            'parent_id' => Yii::t('post', 'Parent ID'),
            'site_disabled' => Yii::t('post', 'Site disabled'),
            'nav_disabled' => Yii::t('post', 'Navigation disabled'),
            'nav_sort' => Yii::t('post', 'Navigation sort'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPostSectionRelations()
    {
        return $this->hasMany(PostSectionRelation::className(), ['section_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['id' => 'post_id'])->via('postSectionRelations');
    }
}
