<?php

namespace ict\posts\common\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property integer $number
 * @property string $alpha
 * @property integer $calling
 * @property string $name_en
 * @property string $name_ru
 * @property string $name_uk
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'alpha', 'calling', 'name_en', 'name_ru', 'name_uk'], 'required'],
            [['number', 'calling'], 'integer'],
            [['alpha'], 'string', 'max' => 2],
            [['name_en', 'name_ru', 'name_uk'], 'string', 'max' => 255],
            [['alpha'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('post', 'ID'),
            'number' => Yii::t('post', 'Number'),
            'alpha' => Yii::t('post', 'Alpha'),
            'calling' => Yii::t('post', 'Calling'),
            'name_en' => Yii::t('post', 'Name En'),
            'name_ru' => Yii::t('post', 'Name Ru'),
            'name_uk' => Yii::t('post', 'Name Uk'),
        ];
    }
}
