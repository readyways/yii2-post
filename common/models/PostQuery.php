<?php
/**
 * Created by PhpStorm.
 * User: turko_v
 * Date: 14.09.2015
 * Time: 17:24
 */

namespace ict\posts\common\models;
use creocoder\taggable\TaggableQueryBehavior;
use yii\db\Expression;


class PostQuery extends \yii\db\ActiveQuery
{
    public function behaviors()
    {
        return [
            TaggableQueryBehavior::className(),
        ];
    }

    /**
     * @return $this
     */
    public function published()
    {
        return $this->andWhere(['>=', 'published' => new Expression('NOW()')]);
    }
}