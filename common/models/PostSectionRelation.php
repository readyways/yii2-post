<?php

namespace ict\posts\common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%post_section_relation}}".
 *
 * @property integer $id
 * @property integer $post_id
 * @property integer $section_id
 *
 * @property Post $post
 * @property Section $section
 */
class PostSectionRelation extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%post_section_relation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'section_id'], 'required'],
            [['post_id', 'section_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('post', 'ID'),
            'post_id' => Yii::t('post', 'Post ID'),
            'section_id' => Yii::t('post', 'Section ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPost()
    {
        return $this->hasOne(Post::className(), ['id' => 'post_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'section_id']);
    }
}
